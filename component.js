!function(name, scope) {
    function Component(name) {
        return function(name) {
            var self = document.createElement(name || "result"), proto = this.constructor.prototype;
            for (var method in proto) self.constructor.prototype[method] = proto[method];
            return self;
        }.apply(this, [ name ]);
    }
    (scope || window)[name] = Component;
}("Component", window);