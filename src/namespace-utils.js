/**Get Repository Structure as a path {string}
 *@memberof global
 *@type {object}
 *@returns {object} object
 **/
namespace.toRepositoryStructure = function(  ) {
    return namespace.normalizeObject(namespace.current, true);
};

/**toStructure
 *@memberof global
 *@type {object}
 *@param {string} path -  package name in dot notation
 *@param {object} target - optional, if specyfied changed are made to the target object.
 *@returns {object} object
 **/
namespace.toStructure = function(path, target) {

    //path = ( (this instanceof Function === true) ? this : path ).toString().split(".");//to allow use a String.prototype straight
    path = ( path ).toString().split(".");

    var current = target || {};
    var containerClone = current;

    while (path.length > 0) {
        
        var name = path.shift();
        current[name] = current[name] || {};
        current = current[name];
    }

    return containerClone;
};

/**Get Object Reference.
 *@memberof global
 *@type {function}
 *@param {string} name - name
 *@param {string} scope - scope
 *@returns {function} object - 
 **/
namespace.getReference = function(name, scope) {
    return Function([], "return arguments[0]" + (name ? "." + name : ""))(scope);
};