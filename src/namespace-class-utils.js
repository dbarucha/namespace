/**
 * Declare a Class.
 * @memberof global
 * @constructor
 * @type {Function}
 */
namespace.Class = function(data){
    //declare local properties
    if(!data){data = { __name: ("class_" + Date.now()) }; }

    var SUPER = data.__super || Function;
    var ARGUMENTS = data.__arguments;
    var BEFORE = data.__before;
    var AFTER = data.__after;
    var NAME = data.__name;
    var PACKAGE = data.__package;
    var REQUIRED = data.__require||null;
    var STATIC = data.__static;
    var NAMESPACE = data.__namespace;
    var CONSTRUCTOR = data.__construct || function( ) {};
    
    //remove unwanted properties
    delete data.__super;
    delete data.__arguments;
    delete data.__package;
    delete data.__before;
    delete data.__require;
    delete data.__static;
    delete data.__name; 
    delete data.__construct;
    delete data.__after;

    if(REQUIRED){
        for(var r in REQUIRED){
            namespace.requireClass(REQUIRED[r]);
        }
        //namespace.requireClass(REQUIRED[r]);
       // return false;
    }
    //declare prototype object
    var proto;
    
    //assign to proto new instance of SUPER function(with arguments ARGUMENTS)
    if( ARGUMENTS ){
        ARGUMENTS instanceof Array ? SUPER.apply(SUPER,ARGUMENTS) : SUPER.apply(SUPER,[ARGUMENTS[0]]);
        proto = SUPER;
    }else{
        proto = new SUPER();
    }
    
    //define local namespace object
    var localNamespace = (typeof NAMESPACE == "string" ? namespace(NAMESPACE) : NAMESPACE) || namespace.current;
    //assign local namespace to constructor to have access later as @property __namespace
    CONSTRUCTOR.__namespace = localNamespace;

    //call callback function before construction
    !BEFORE||BEFORE(CONSTRUCTOR);
    //extend prototype using SUPER.prototype as 'donor'
    namespace.extendObject(proto,SUPER.prototype,true);
    
    //extend prototype using data param as 'donor'
    namespace.extendObject(proto,data);
    
    //create prototype constructor
    proto.constructor = CONSTRUCTOR;
    
    //extend constructor using data.static {object} as 'donor'
    !STATIC||namespace.extendObject(CONSTRUCTOR,STATIC);
    
    //assign super function to prototype to have access later as @property __super
    //proto.__super = SUPER;
    
    //assign name to prototype to have access later as @property __name
    if (NAME) proto.__name = NAME;
        
    //implement toString prototype method
    proto.toString = function(){
        return arguments.callee.constructor.prototype.toString();
    };
    
    //construct class
    CONSTRUCTOR.prototype = proto;
    CONSTRUCTOR.prototype.__super = SUPER;
        //assign parent to prototype to have access later as @property __parent
    CONSTRUCTOR.prototype.__parent = SUPER ? (SUPER.prototype.__super||SUPER) : proto;
    
    //add constructor to local namespace
    !PACKAGE || (CONSTRUCTOR.__package = PACKAGE) && (CONSTRUCTOR.prototype.__package = CONSTRUCTOR.__package);
    var space = CONSTRUCTOR.__package || CONSTRUCTOR.__namespace;
    ( space[NAME] || (space[NAME] = CONSTRUCTOR) );
    CONSTRUCTOR.prototype.__namespace = CONSTRUCTOR.__namespace;
    
    //call callback function after construction
    !AFTER||AFTER(CONSTRUCTOR);

    //cache 'last class'
    namespace.$[NAME] = namespace.$[NAME] || CONSTRUCTOR;
    
    return CONSTRUCTOR;
};