/**
 * Declare a Package.
 * @memberof global
 * @constructor
 * @type {Function}
 */
namespace.Package = function(ns, name, jsclass){

	var pkg = name.split("."),
    localNamespace = namespace(ns),
    lastName = pkg[pkg.length-1];

    name.length <= 1 || namespace.toStructure(name, localNamespace);
    var packageSpace = namespace.findObject(lastName, localNamespace);
    jsclass.__package = packageSpace;
    jsclass.__namespace = localNamespace;

    return namespace.Class(jsclass).__package;
};