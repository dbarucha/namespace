/**
 * URLRequest description
 * @memberof global
 * @type {Object}
 * @param {String} method
 * @param {String} url
 * @returns {Object} XMLHttpRequest
 */
namespace.URLRequest = function(method, url) {
    var XMLHTTP = ( XMLHttpRequest||( ActiveXObject("Msxml2.XMLHTTP")||ActiveXObject("Msxml2.XMLHTTP.6.0")||ActiveXObject("Msxml3.XMLHTTP")||ActiveXObject("Microsoft.XMLHTTP") ) );
    var xhr = new XMLHTTP();
        if ("withCredentials" in xhr) {
            // XHR for Chrome/Firefox/Opera/Safari.
            xhr.open(method, url, true);
        } else if (typeof XDomainRequest != "undefined") {
            // XDomainRequest for IE.
            xhr = new XDomainRequest();
            xhr.open(method, url);
        } else {
            // CORS not supported.
            xhr = null;
        }      
    return xhr;
};

/**
 * _URL description
 * @private
 * @memberof global
 * @type {undefined}
 * @param {String} href - href
 */
namespace._URL = function(href) {
    var url = document.createElement("a");
    url.href = href;
    return url;
};


/**
 * fileGetContent description
 * @memberof global
 * @type {undefined}
 * @param {String}
 * @param {Function}
 * @param {Function}
 */
namespace.fileGetContent = function(path, onLoad, onError) {
	var url = namespace._URL( path );
	if(url){
		var XMLHTTP = namespace.URLRequest("GET", url.href, false );

	    XMLHTTP.onload = onLoad;
	    XMLHTTP.onerror = onError;
	    XMLHTTP.send();

	    return XMLHTTP; 
	}

	return;
};

/**
 * Imports a class into current or desired namespace
 * @memberof global
 * @type {Boolean}
 * @param {String} nspath
 * @param {String} ns
 * @returns {Boolean}
 */
namespace.importClass = function(nspath, ns){
    var n = nspath.split(".");
    var className = n.pop();
    var s = arguments.callee.settings;
    var url = namespace._URL((s.paths.repository ? s.paths.repository + namespace.DIR_SEPARATOR:"") + n.join(namespace.DIR_SEPARATOR) + namespace.DIR_SEPARATOR + className + s.ext);

    if(url.href){
    namespace.fileGetContent(url.href, function(e){
        if(parseInt(e.target.status) < 400){
            namespace( ns||(n.shift() + "." + n.shift()) );
            (new Function(e.target.responseText)).apply(this);
        }
    }, function(e){console.log("ERROR",e);});
    }else{
        return false;
    }

    return true;
};

/**
 * importClass settings
 * @memberof global
 * @type {Object}
 * @default
 */
namespace.importClass.settings = {
    ext:".js",
    paths:{
        repository: null
    }
};