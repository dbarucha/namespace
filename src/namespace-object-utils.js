/**
 * Extend an object - copy all donor properties.
 *@memberof global
 *@type {object}
 *@param {object} recipient - object to copy to
 *@param {object} donor - object to copy from
 *@param {boolean} check - if set to true then all existing recipient's properties are not overwritten.
 *@returns {object} recipient
 **/
namespace.extendObject = function(recipient, donor, check) {
    for (var i in donor) {
        check === true ? (recipient[i] || (recipient[i] = donor[i])) : (recipient[i] = donor[i]);
    }
    return recipient;
};

/**
 * Find an object as property/method of another.
 *@memberof global
 *@type {object|boolean}
 *@param {object} needle - object to search for
 *@param {object} haystack - object to search in
 *@returns {object|boolean} - returns object if found or false otherwise.
 **/
namespace.findObject = function(needle, haystack) {

    for (var key in haystack) {
        if (needle === key) {
            return haystack[key];
        }

        if (haystack[key].constructor === Object) {
            var out = arguments.callee(needle, haystack[key]);
            if (out) {
                return out;
            }
        }
    }


    return false;
};

/**
 * Normalize an object using JSON with replacer.
 *@memberof global
 *@type {object|string}
 *@param {object} target - object to normalize
 *@param {boolean} stringify - optional,
 *@returns {object|string} - returns string if stringify equals boolean true or JSON object otherwise.
 *@see http://msdn.microsoft.com/en-us/library/cc836459(v=vs.94).aspx
 *@see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON/stringify
 **/
namespace.normalizeObject = function(target, stringify) {
    var cache = [];
    var str = JSON.stringify(target, function(key, value) {
        if (value instanceof Object && value !== null) {
            if (cache.indexOf(value) !== -1) {
                return stringify === true ? value.toString() : typeof value;
            }
            cache.push(value);
        }
        return value;
    });
    cache = null;
    return stringify === true ? str : JSON.stringify(str);
};


/**
 * Walks recursively through an object calling callback function every item.
 *@memberof global
 *@type {Object}
 *@param {object} tree - an object to walk through
 *@param {function} callback - function to call every item, passing arguments:treeNode, node, tree, coords[{offset: 0, index: 0, counter: 0}]
 *@returns {object} target - returns modified target
 **/
namespace.walkObjectRecursively = function(tree, callback) {

    var coords = arguments[2] || {offset: 0, index: 0, counter: 0};

    arguments.callee.lastIndex || (arguments.callee.lastIndex = coords.index);

    if (arguments.callee.deepLevelMax >= coords.offset) {

        for (var node in tree) {

            coords.counter++;
            !callback || callback.apply(callback, [tree[node], node, tree, {
                    offset: coords.offset,
                    index: coords.index,
                    counter: coords.counter
                }]);
            coords.index++;

            if (tree[node] instanceof  Object) {
                arguments.callee.lastIndex = coords.index;
                coords.offset++;
                coords.index = 0;
                arguments.callee(tree[node], callback, coords);
                coords.index = arguments.callee.lastIndex;
            }
        }

    }
    coords.offset === 0 || coords.offset--;

    return tree;
};
/**
 * deepLevelMax description
 * @memberof global
 * @type {Number}
 * @default
 */
namespace.walkObjectRecursively.deepLevelMax = 9;
/**
 * findObjectParents description
 * @memberof global
 * @type {Object}
 * @param {Object} target - target
 * @param {Object} tree - tree
 * @param {Boolean} firstOnly - if true returns first parent only
 */
namespace.findObjectParents = function(target, tree, firstOnly) {
    var parentName = "", result = [], root = tree;

    function iterate(tree) {
        var parentPrevName = "";

        for (var property in tree) {
            if (tree.hasOwnProperty(property)) {

                if (tree[property].constructor === Object) {
                    parentName = property;
                    root = tree;
                    arguments.callee(tree[property], parentName);
                    parentName = parentPrevName;
                }

                if (target === property || target === tree[property]) {
                    result.push({target: target, name: parentName, parent: tree, root: root});

                }
            }
        }
    }

    iterate(tree);

    return firstOnly === true ? result.shift() : result;
};