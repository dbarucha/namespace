/**Load script by appending to HEAD or parent passed as the param.
 *@memberof global
 *@param {string} url - script URL
 *@param {function} callback - function to call when script is loaded
 *@param {object} parent
 *@returns {object} 
 **/
namespace.loadScript = function(url, callback, parent) {
    var script = document.createElement("script");
    script.type = "text/javascript";
    script.src = url;
    !(parent && parent.parentNode) ? document.getElementsByTagName("head")[0].appendChild(script) : parent.parentNode.insertBefore(script, parent.firstChild);
    if (callback instanceof Function) {
        script.attachEvent || (script.onload = function(e) {
            callback(e);
        });
        script.onreadystatechange = function(e) {
            if (script.readyState == namespace.EVENT_COMPLETE)
                callback(e);
            return script;
        };
    }
    return script;
};