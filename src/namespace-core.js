scope || (scope = window);
/**Base namespace functionality. 
 *@constructor
 *@memberof global
 *@param {object} namespace - namespace's name
 *@param {object} parent - parent namespace object
 *@param {boolean} silent - if true the namespace is created only, becoming 'current' otherwise
 *@returns {object} - current namespace
 *@type {Function}
 **/
var namespace = function(namespaceName, structure, silent) {
    if (!namespaceName || namespaceName instanceof Object) {
        return false;
    }
    var _name = namespaceName.toUpperCase().replace(/[^\w]/g, "_");
    var constructor = scope[name];
    constructor.N$[_name] || (constructor.N$[_name] = namespaceName);

    constructor.namespaces = constructor.namespaces || {};
    constructor.namespaces[namespaceName] || (constructor.namespaces[namespaceName] = {});
    constructor.packages || (constructor.packages = {});
    constructor.namespaces[namespaceName].__name = _name;
    
    constructor.toStructure(namespaceName, constructor.packages);//>=0.3.2
    var lastName = namespaceName.split(".").pop();//>=0.3.2
    var packageSpace = namespace.findObject(lastName, constructor.packages);//>=0.3.2
    !structure || constructor.extendObject(packageSpace, structure);//>=0.3.2

    !structure || constructor.extendObject(constructor.namespaces[namespaceName], structure);//<=0.3.1
   
    silent === true || (constructor.current = constructor.namespaces[namespaceName]);

    return silent === true ? constructor.namespaces[namespaceName] : constructor.current;
};
scope[name] = namespace;

/**Directory separator
 *@constant
 *@memberof namespace
 **/
namespace.DIR_SEPARATOR = "/";
/**Event type: abort
 *@constant
 *@memberof namespace
 **/
namespace.EVENT_ABORT = "abort";
/**Event type: complete
 *@constant
 *@memberof namespace
 **/
namespace.EVENT_COMPLETE = "complete";
/**Event type: error
 *@constant
 *@memberof namespace
 **/
namespace.EVENT_ERROR = "error";
/**Event type: load
 *@constant
 *@memberof namespace
 **/
namespace.EVENT_LOAD = "load";
/**Event type: progress
 *@constant
 *@memberof namespace
 **/
namespace.EVENT_PROGRESS = "progress";
/**Event type: timeout
 *@constant
 *@memberof namespace
 **/
namespace.EVENT_TIMEOUT = "timeout";

/**Reserved.
 *@default
 *@memberof namespace
 **/
namespace.$ = {};
/**Reserved.
 *@default
 *@memberof namespace
 **/
namespace.N$ = {};