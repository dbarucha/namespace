
    namespace.require = function(a) {
        var r = a && a instanceof Array ? a : arguments;
        var q = arguments.callee.queues;
        for (var i in r) {
            if (!q.required[r[i]]) {
                q.requiredQueue.push(r[i].split(".").join("/") + ".js");
                q.required[r[i]] = Date.now()
            }
        }
        arguments.callee.dequeue()
    };
    namespace.require.queues = {required: {}, requiredQueue: [], scriptQueue: []};
    namespace.require.queueBusy = false;
    namespace.enqueueScript = function(url, callback) {
        namespace.require.queues.scriptQueue.push([url, callback]);
        namespace.require.dequeue()
    };
    namespace.require.dequeue = function() {
        if (namespace.require.queues.requiredQueue.length > 0) {
            namespace.loadScript(namespace.require.queues.requiredQueue.shift(), arguments.callee);
            return true
        } else {
            if (namespace.require.queues.scriptQueue.length > 0) {
                var i = namespace.require.queues.scriptQueue.shift();
                namespace.loadScript(i[0], i[1]);
                arguments.callee();
                return true
            }
        }
        return false
    };
 
(function(namespace) {
    namespace.http || (namespace.http = {});
    namespace.METHOD_CONNECT = "CONNECT";
    namespace.METHOD_DELETE = "DELETE";
    namespace.METHOD_GET = "GET";
    namespace.METHOD_HEAD = "HEAD";
    namespace.METHOD_POST = "POST";
    namespace.METHOD_PUT = "PUT";
    namespace.METHOD_OPTIONS = "OPTIONS";
    namespace.http.XHR = function() {
        this.timestamp = Date.now();
        this.method = "";
        this.response = {};
        var statusListeners = {};
        var stateListeners = {};
        this.headers = {};
        this.request = new (XMLHttpRequest || (ActiveXObject("Msxml2.XMLHTTP") || (ActiveXObject("Msxml2.XMLHTTP.6.0") || (ActiveXObject("Msxml3.XMLHTTP") || ActiveXObject("Microsoft.XMLHTTP")))));
        var self = this;
        this.setRequestHeader = function(name, value) {
            this.headers[name] = value
        };
        this.unsetRequestHeader = function(name, value) {
            if (!value) {
                this.headers[name] = []
            } else {
                var l = [];
                while (this.headers[name].length) {
                    var item = this.headers[name].pop();
                    value && item == value || l.push(item)
                }
                this.headers[name] = l
            }
        };
        this.addStatusListener = function(status, listener, onetime) {
            var s = parseInt(status);
            statusListeners[s] || (statusListeners[s] = []);
            statusListeners[s].push({listener: listener, onetime: onetime});
            return this
        };
        this.addStateListener = function(state, listener, onetime) {
            stateListeners[state] || (stateListeners[state] = []);
            stateListeners[state].push({listener: listener, onetime: onetime});
            return this
        };
        this.removeStatusListener = function(status, listener) {
            var s = parseInt(status);
            if (!listener) {
                statusListeners[s] = []
            } else {
                var l = [];
                while (statusListeners[s].length) {
                    var item = statusListeners[s].pop();
                    listener && item.listener == listener || l.push(item)
                }
                statusListeners[s] = l
            }
        };
        this.removeStateListener = function(state, listener) {
            if (!listener) {
                stateListeners[state] = []
            } else {
                var l = [];
                while (stateListeners[state].length) {
                    var item = stateListeners[state].pop();
                    listener && item.listener == listener || l.push(item)
                }
                stateListeners[state] = l
            }
        };
        this.requestEventHandler = function(event) {
            enumerateListenersByType(event.type, event, stateListeners)
        };
        this.requestStateHandler = function(event) {
            if ( event.target && ((event.target && event.target.readyState == 4) || ((window.location.protocol == "file:" && event.target.status == 200)))) {
                enumerateListenersByType(event.target.status, event, stateListeners);
                enumerateListenersByType(event.type, event, stateListeners);
                if (event.target.status < 400) {
                    enumerateListenersByType(namespace.EVENT_COMPLETE, event, stateListeners);
                } else {
                    enumerateListenersByType(namespace.EVENT_ERROR, event, stateListeners);
                }
            } else {
                 
            }
        };
        this.request.onreadystatechange = this.requestStateHandler;
        this.request.onerror = this.requestEventHandler;
        this.request.onabort = this.requestEventHandler;
        this.request.onprogress = this.requestEventHandler;
        this.request.ontimeout = this.requestEventHandler;
        function request(method, url, params, urlescape) {
            this.method = method;
            this.request.open(method, url, params && params.async || true);
            this.request.timeout = params && params.timeout || 0;
            var h = this.headers;
            !(params && params.headers) || namespace.extendObject(h, params.headers);
            if (this.request.setRequestHeader) {
                for (var i in h) {
                    this.request.setRequestHeader(i, h[i])
                }
            }
            var urldata = [];
            if (params && params.data) {
                for (var d in params.data) {
                    urldata.push(d + "=" + (urlescape === true ? encodeURIComponent(params.data[d]) : params.data[d]))
                }
            }
            this.request.send()
        }
        function enumerateListenersByType(type, event, listeners) {
            if (listeners && listeners[type]) {
                for (var i in listeners[type]) {
                    !listeners[type].listener instanceof Function || listeners[type][i].listener(event)
                }
            }
        }
        this.GET = function(url, params, urlescape) {
            request.apply(this, [namespace.METHOD_GET, url, params, urlescape])
        };
        this.get = this.GET;
        this.POST = function(url, params, urlescape) {
            request.apply(this, [namespace.METHOD_POST, url, params, urlescape])
        };
        this.PUT = function(url, params, urlescape) {
            request.apply(this, [namespace.METHOD_PUT, url, params, urlescape])
        };
        this.DELETE = function(url, params, urlescape) {
            request.apply(this, [namespace.METHOD_DELETE, url, params, urlescape])
        };
        this.HEAD = function(url, params, urlescape) {
            request.apply(this, [namespace.METHOD_HEAD, url, params, urlescape])
        };
        this.OPTIONS = function(url, params, urlescape) {
            request.apply(this, [namespace.METHOD_OPTIONS, url, params, urlescape])
        }
    };
    namespace.XHR = namespace.http.XHR;
    namespace.http.loadURL = function(url, callback) {
        var r = new namespace.http.XHR;
        r.addStateListener(namespace.EVENT_COMPLETE, callback);
        r.get(url)
    };
    namespace.loadURL = namespace.http.loadURL;
    namespace.http.loadJSON = function(url, callback) {
        namespace.http.loadURL(url, function(e) {
            callback(e.target.responseText ? JSON.parse(e.target.responseText) : null)
        })
    };
    namespace.loadJSON = namespace.http.loadJSON
})(namespace);