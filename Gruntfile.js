/*global module:false*/

module.exports = function(grunt) {
  var basePath = '/';
  var sourcePath = 'src/';
  var buildPath = 'build/';
  var distPath = 'dist/';
  var configPath = 'config/';
  var docsPath = 'docs/';

  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'),

    uglify: {
      beautify: {
        options: {
          beautify: {
            beautify: true
          },
          mangle: false,
          preserveComments: "all"
        },
        files: [{
            expand: true,
            cwd: 'src',
            src: '*.js',
            dest: 'build'
        }]
      },
      dist: {
        options: {
          beautify: {
            beautify: false,
          },
          compress: {
            dead_code: true
          },
          mangle: true
        },
        files: {
            'dist/namespace.min.js': 'dist/namespace.js'
        }
      }
    },
    
    // Clear previous compiled files.
    clean: {
        js: {
            src: [buildPath, distPath]
        },
        specs: {
            src: ['test/compiled']
        }
    },

    watch: {
      jshint: {
            files: [sourcePath + '*.js', 'Gruntfile.js', 'test/**/*.js'],
            tasks: ['jshint']
      },
      dev: {
            files: [sourcePath + '**/*.js'],
            tasks: ['dev']
      }
    },

    copy: {
          copyToLocalhost: {
              files: [{
                  expand: true,
                  cwd: buildPath,
                  src: ['code.js'],
                  dest: 'c:/wamp/www/namespace/js',
                  filter: 'isFile'
              },
              {
                  expand: true,
                  cwd: distPath,
                  src: ['*'],
                  dest: 'c:/wamp/www/namespace/dist',
                  filter: 'isFile'
              },
              {
                  expand: true,
                  cwd: distPath,
                  src: ['**/*'],
                  dest: 'c:/wamp/www/namespace/dist',
                  filter: 'isFile'
              },
              {
                  expand: true,
                  cwd: "./",
                  src: ['index.html'],
                  dest: 'c:/wamp/www/namespace',
                  filter: 'isFile'
              },
              {
                  expand: true,
                  cwd: "./",
                  src: ['app-config.js'],
                  dest: 'c:/wamp/www/namespace',
                  filter: 'isFile'
              }]
          },
          copyToRemote: {
              files: [{
                  expand: true,
                  cwd: distPath,
                  src: ['**/*'],
                  dest: 'd:/remote/namespace',
                  filter: 'isFile'
              },{
                  expand: true,
                  cwd: docsPath,
                  src: ['**/*'],
                  dest: 'd:/remote/namespace/documentation'
              }]
          }
      },

      createBundle: {
        namespace: {
            paths: [
              "build/namespace-core.js",
              "build/namespace-utils.js",
              "build/namespace-object-utils.js",
              "build/namespace-url-utils.js",
              "build/namespace-code-utils.js",
              "build/namespace-class-utils.js",
              "build/namespace-package-utils.js"
            ],
            output: distPath + 'namespace.js'
        }
    },

    jsdoc : {
      dist : {
          src: ['dist/namespace.js', 'README.md'], 
          options: {
              destination: 'docs',
              template : configPath + "docs/templates/default"
          }
      }
    },

    jshint: {
      options: {
          reporter: require('jshint-stylish'),
          evil: true,
          expr: true
      },
      all: ['Gruntfile.js', 'src/**/*.js', 'test/**/*.js']
  }

});

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-jsdoc');


  /*grunt.registerTask('default', [
        'compile-js','jsdoc','copy:copyToRemote'
  ]);*/  
  grunt.registerTask('default', [
        'uglify:beautify','jshint:all','createBundle','jsdoc','uglify:dist'
  ]);

  grunt.registerTask('dev', [
        'uglify:beautify','jshint:all','createBundle','uglify:dist','copy:copyToRemote','copy:copyToLocalhost'
  ]);

  grunt.registerTask('compile-js', [
        'uglify:beautify','jshint:all','createBundle','jsdoc','uglify:dist','copy:copyToRemote'
  ]);

  grunt.registerMultiTask("createBundle", "create namespace as a bundle", function() {
    var paths = grunt.file.expand( this.data.paths ),
        out = this.data.output,
        contents = ((grunt.file.read("LICENSE.txt")+ "\n\n\n")||"") + "\n/**\n*@name global\n*@namespace global\n*/\n",
        contentPrefix = "(function(name, scope) {\n",
        contentSufix = this.target ? "\n})(\"" + this.target + "\", this);" : "";


    var required = this.data.require ? grunt.file.expand( this.data.require ) : [];
    required.forEach(function( filepath ) {
        if ( /\.js$/i.test( filepath ) ) {
          var fileContent = grunt.file.read(filepath);
          contents += "\n/**" + filepath + "*/\n" + fileContent+ "\n";
          contents += "\n\n" + fileContent+ "\n\n";
        } 
    });
    contents += contentPrefix;
    paths.forEach(function( filepath ) {
        if ( /\.js$/i.test( filepath ) ) {
          var fileContent = grunt.file.read(filepath);
          contents += "\n/**" + filepath + "*/\n" + fileContent+ "\n";
          //contents += "\n\n" + fileContent+ "\n\n";
        } 
    });

    contents += contentSufix;

    grunt.file.write( out, contents );
});

};