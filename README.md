# namespace aka dandruid-namespace
Base namespace functionality. 

## Usage
```js
var NS = namespace("com.dandruid").current;
NS.myMethod = function(){return "is awesome!";}
```

```js
namespace("com.jquery", require(jQuery));
//namespace.current == jQuery
```

```js
namespace("com.dandruid", {
	models: {},
	controllers: {},
	views: {},
	templates: {}
});
```